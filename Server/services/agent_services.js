const express = require('express');
const agentRoutes = require('../routes/agent_routes');
const app = express();
const port = 3001;

//middleware
app.use(express.json());

app.get("/", (req, res) => {
    res.send(`Agent services server is running on post: ${port}`)
});

app.use('/api/v1/agent', agentRoutes);

//Start agent server
const startServer = (port) => {
    app.listen(port, () => {
        console.log(`Agent sub server is running on post: ${port}`)
    });
};

module.exports = { startServer };