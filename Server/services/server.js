// need to add server security methods....
const express = require("express");
const appRoutes = require("../routes/routes");
const app = express();
const port = 3000;

//middleware
app.use(express.json());

app.get("/", (req, res) => {
    res.send(`Trip services server runnig on port ${port} `)
});

app.use('/api/v1/trip', appRoutes);

//start server
const startServer = (port) => {
    app.listen(port, () => {
        console.log(`Trip sub server is running on post: ${port}`)
    });
};

module.exports = { startServer };