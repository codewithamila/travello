// retrive trip data queries
const getTrips = "SELECT * FROM Trip";
const getTripsbyId = "SELECT * FROM trip WHERE id = $1";
const getTripsbyDestinations = "SELECT * FROM trip WHERE destinations = $1";
const addTrip = "INSERT INTO trip (package_name, destinations, vehicle_model, passenger_capacity, days, price) VALUES ($1, $2, $3, $4, $5, $6)";
const deleteTrip = "DELETE FROM trip WHERE id = $1";
const updateTrip = "UPDATE trip SET package_name = $1, destinations = $2, vehicle_model = $3, passenger_capacity = $4, days = $5, Price = $6 WHERE id = $7";


module.exports = {
  getTrips,
  getTripsbyId,
  getTripsbyDestinations,
  addTrip,
  deleteTrip,
  updateTrip,
};