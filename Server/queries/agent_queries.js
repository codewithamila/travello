//retrive agent data queries
const getAllAgents = "SELECT * FROM agents";
const getAgentById = "SELECT * FROM agents WHERE id = $1";
const deleteAgentById = "UPDATE agents SET isDeletedAgent = TRUE WHERE id = $1"
const updateAgentById = "UPDATE agents SET name = $1, email = $2, phone_number = $3, username = $4, password = $5 WHERE id = $6";
const addAgent = "INSERT INTO agents (name, email, phone_number, username, password) VALUES ($1, $2, $3, $4, $5)";
const checkIsDeletedAgent = "SELECT isDeletedAgent FROM agents WHERE id = $1";

module.exports = {
    getAllAgents,
    getAgentById,
    deleteAgentById,
    updateAgentById,
    addAgent,
    checkIsDeletedAgent,
};