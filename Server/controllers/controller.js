const pool = require("../databases/db");
const queries = require("../queries/queries");

//get trips api
const getTrips = (req, res) => {
  pool.query(queries.getTrips, (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

//get trips by id
const getTripsbyId = (req, res) => {
  const id = parseInt(req.params.id);

  pool.query(queries.getTripsbyId, [id], (error, results) => {
    if (error) {
      console.error(error);
      res.status(500).send("Internal server error get trips by id!...");
    } else {
      if (results.rows.length === 0) {
        res.status(404).send("Trip details not found!");
      } else {
        res.status(200).json(results.rows[0]);
      }
    }
  });
};

// get all agents
const getAllAgents = (req, res) => {
  pool.query(queries.getAllAgents, (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  })
};


//search api destination
const getTripsbyDestinations = (req, res) => {
  const destination = req.params.destinations.toLowerCase();

  pool.query(
    queries.getTripsbyDestinations,
    [destination],
    (error, results) => {
      if (error) {
        console.error(error);
        res.status(500).send("Internal server error get trips by destination!...");
      } else {
        if (results.rows.length === 0) {
          res.status(404).send("Trip details not found!");
        } else {
          res.status(200).json(results.rows);
        }
      }
    }
  );
};

//add trip api
const addTrip = (req, res) => {
  const {
    package_name,
    destinations,
    vehicle_model,
    passenger_capacity,
    days,
    price,
  } = req.body;
  console.log("Received data:", req.body);
  pool.query(
    queries.addTrip,
    [
      package_name,
      destinations,
      vehicle_model,
      passenger_capacity,
      days,
      price,
    ],
    (error, results) => {
      if (error) {
        throw error;
      }
      res.status(200).send("Trip added successfully...");
    }
  );
};

//delete trip api
const deleteTrip = (req, res) => {
  const id = parseInt(req.params.id);
  pool.query(queries.deleteTrip, [id], (error, results) => {
    if (error) {
      throw error;
    } else {
      const deletedRowCount = results.rowCount;
      if (deletedRowCount === 0) {
        res.status(404).send("Trip does not exists!");
      } else {
        res.status(200).send("Trip deleted successfully...");
      }
    }
  });
};

//update trip api
const updateTrip = (req, res) => {
  const id = parseInt(req.params.id);
  const {
    package_name,
    destinations,
    vehicle_model,
    passenger_capacity,
    days,
    price,
  } = req.body;

  pool.query(queries.getTripsbyId, [id], (error, results) => {
    if (results.rows.length === 0) {
      res.send("Trip details not exists!");
    } else {
      const existingTrip = results.rows[0];
      const updateTrip = {
        package_name: package_name || existingTrip.package_name,
        destinations: destinations || existingTrip.destinations,
        vehicle_model: vehicle_model || existingTrip.vehicle_model,
        passenger_capacity:
          passenger_capacity || existingTrip.passenger_capacity,
        days: days || existingTrip.days,
        price: price || existingTrip.price,
      };
      pool.query(
        queries.updateTrip,
        [
          updateTrip.package_name,
          updateTrip.destinations,
          updateTrip.vehicle_model,
          updateTrip.passenger_capacity,
          updateTrip.days,
          updateTrip.price,
          id,
        ],
        (error, results) => {
          if (error) {
            res.status(500).send("Internal server error!");
          } else {
            res.status(200).send("Trip details updated...");
          }
        }
      );
    }
  });
};







module.exports = {
  getTrips,
  getTripsbyId,
  getTripsbyDestinations,
  addTrip,
  deleteTrip,
  updateTrip,

  getAllAgents,
};
