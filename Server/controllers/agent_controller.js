const pool = require('../databases/db');
const queries = require('../queries/agent_queries');
const bcrypt = require('bcrypt');
const validator = require('../validatiors/validators');

//get all agents
const getAllAgents = (req, res) => {
    pool.query(queries.getAllAgents, (error, results) => {
        if (error) {
            console.error(error)
            res.status(500).send("Internal server error in get all agents!")
        } else {
            const filteredRows = results.rows.filter(row => !row.isdeletedagent)
            res.status(200).json(filteredRows)
        }
    })
}

// get agent by id
const getAgentById = (req, res) => {
    const id = parseInt(req.params.id)
    pool.query(queries.getAgentById, [id], (error, results) => {
        if (error) {
            console.error(error)
            res.status(500).send("Internal error in get agent!")
        } else {
            if (results.rows.length === 0) {
                res.status(404).send('Agent not found!')
            } else {
                res.status(200).json(results.rows[0])
            }
        }
    })
};


//check is deleted agent
const checkIsDeletedAgent = (req, res) => {
    const id = parseInt(req.params.id)
    pool.query(queries.checkIsDeletedAgent, [id], (error, results) => {

        if (error) {
            console.error(error)
            res.status(500).send('Internal server error in check is agent deleted!')
        } else {
            if (results.rows.length === 0) {
                res.status(404).send("Agent delails not found")
            } else {
                res.status(200).json(results.rows[0])
            }
        }
    })
};


//delete agent by id
const deleteAgentById = (req, res) => {
    const id = parseInt(req.params.id);
    pool.query(queries.checkIsDeletedAgent, [id], (error, results) => {
        if (error) {
            console.error(error);
            res.status(500).send("Internal server error in delete agent!");
        } else {
            if (results.rows.length === 0) {
                console.log("Agent not found!");
                res.status(404).send("Agent not found!");
            } else {
                const isDeleted = results.rows[0].isdeletedagent;
                console.log("Agent is deleted:", isDeleted);
                if (isDeleted) {
                    res.status(404).send("Agent already deleted!");
                } else {
                    pool.query(queries.deleteAgentById, [id], (error, results) => {
                        if (error) {
                            console.error(error);
                            res.status(500).send("Error deleting agent!");
                        } else {
                            res.status(200).send('Agent deleted successfully...');
                        }
                    });
                }
            }
        }
    });
};


//add agent api
const addAgent = (req, res) => {
    const {
        name,
        email,
        phone_number,
        username,
        password,
    } = req.body;

    // validations calling
    if (!(validator.isValidEmail(email))) {
        //console.error(error)
        return res.status(400).send("Invalid email address format!")
    }
    if (!(validator.isValiedPhoneNumber(phone_number))) {
        //console.error(error)
        return res.status(400).send("Phone number should start with '0' or '+' !")
    }
    if (!(validator.isValidPassword(password))) {
        //console.error(error)
        return res.status(400).send("Password should be at lease 8 digits!")
    }

    //password bcrypt
    bcrypt.hash(password, 10, (error, hashedPassword) => {
        if (error) {
            console.error(error)
        } else {
            //just for testing this 
            console.log("Recieved data: ", hashedPassword)

            pool.query(queries.addAgent, [
                name,
                email,
                phone_number,
                username,
                hashedPassword,
            ], (error, results) => {
                if (error) {
                    console.error(error)
                    res.status(500).send("internal server error in add agent!")
                } else {
                    res.status(200).send("Agent added successfully...")
                }
            })
        }

    })
};

//update agent by id
const updateAgentById = (req, res) => {
    const id = parseInt(req.params.id)
    const {
        name,
        email,
        phone_number,
        username,
        password,
    } = req.body

    pool.query(queries.getAgentById, [id], (error, results) => {
        if (results.rows.length === 0) {
            res.status(404).send('Agent does not exists..')
        } else {
            const existingAgent = results.rows[0]
            const updateAgentById = {
                name: name || existingAgent.name,
                email: email || existingAgent.email,
                phone_number: phone_number || existingAgent.phone_number,
                username: username || existingAgent.username,
                password: password || existingAgent.password,
            };
            pool.query(queries.updateAgentById, [
                updateAgentById.name,
                updateAgentById.email,
                updateAgentById.phone_number,
                updateAgentById.username,
                updateAgentById.password,
                id,
            ], (error, results) => {
                if (error) {
                    console.error(error)
                    res.status(500).send('internal server error in update agent details!')
                } else {
                    res.status(200).send('Agent details updated...')
                }
            })
        }
    })

};


module.exports = {
    getAllAgents,
    getAgentById,
    updateAgentById,
    addAgent,
    checkIsDeletedAgent,
    deleteAgentById,
}