const { Router } = require('express');
const agentController = require('../controllers/agent_controller');
const agentRouter = Router();

// routes for agent
agentRouter.get("/agents", agentController.getAllAgents);
agentRouter.get("/agents/:id", agentController.getAgentById);
agentRouter.put("/agentdelete/:id", agentController.deleteAgentById);
agentRouter.put("/agentsupdate/:id", agentController.updateAgentById);
agentRouter.post("/agents", agentController.addAgent);
agentRouter.get("/agent/:id", agentController.checkIsDeletedAgent);

module.exports = agentRouter;
