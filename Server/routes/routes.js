const { Router } = require('express');
const controller = require('../controllers/controller');
const router = Router();


// routes for trips
router.get("/trips", controller.getTrips);
router.get("/trips/:id", controller.getTripsbyId);
router.get("/trips/:destinations", controller.getTripsbyDestinations);
router.post("/trips", controller.addTrip);
router.delete("/trips/:id", controller.deleteTrip);
router.put("/trips/:id", controller.updateTrip);

module.exports = router;