
//email validation function
const isValidEmail = (email) =>{
    const emailRegx = /^([a-zA-Z0-9._%-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,})$/;
    return emailRegx.test(email)
};

//password valicaiton function
const isValidPassword = (password)=> {
    if (password.length < 8) {
        return false;
    } else {
        return true;
    }
};

//phone number valication function
const isValiedPhoneNumber = (phone_number) => {
    if (phone_number.charAt(0) !== '0' && phone_number.charAt(0) !== '+') {
        return false
    } else {
        return true
    }
};

module.exports = {
    isValidEmail,
    isValidPassword,
    isValiedPhoneNumber,
}