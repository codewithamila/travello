const agentServer = require('./services/agent_services');
const tripServer = require('./services/server');

const agentServerPort = 3001;
const tripServerPort = 3000;

agentServer.startServer(agentServerPort);
tripServer.startServer(tripServerPort);

console.log("Main server starting....");
console.log(`Agent server starting on port: ${agentServerPort}`);
console.log(`Trip server starting on port: ${tripServerPort}`);